/** @jsx React.DOM */

var React = require('react');
var App = require('./components/app');

var bus = require('./bus');
var state = require('./state');

function render(newState) {
  console.log(newState);
  React.renderComponent(
    <App bus={bus} state={newState} />,
    document.querySelector('main')
  );
}

state.onValue(render);
