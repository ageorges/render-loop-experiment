/** @jsx React.DOM */

var React = require('react');
var Search = require('./search');
var Spinner = require('./spinner');
var Results = require('./results');

module.exports = React.createClass({
  render: function() {
    return (
      <div>
        <Search value={this.props.state.searchTerm} bus={this.props.bus.searchTerm} />
        <Spinner active={this.props.state.loading} />
        <Results plugins={this.props.state.plugins} />
      </div>
    );
  }
});