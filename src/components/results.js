/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({
  render: function() {
    var items = this.props.plugins.map(function(plugin) {
      return <li key={plugin.pluginKey}>{plugin.name}</li>;
    });

    if (this.props.plugins.length > 0) {
      return <ul>{items}</ul>;
    } else {
      return <p>no results</p>;
    }
  }
});
