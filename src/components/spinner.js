/** @jsx React.DOM */

var React = require('react');
var addons = require('react-addons');

module.exports = React.createClass({
  render: function() {
    var classes = addons.classSet({
      'spinner': true,
      'active': this.props.active
    });

    return <span className={classes}></span>;
  }
});