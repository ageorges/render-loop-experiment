/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({
  getInitialState: function() {
    return { term: this.props.value };
  },

  handleChange: function(newTerm) {
    this.setState({ term: newTerm}, function() {
      this.props.bus.push(newTerm);
    });
  },

  render: function() {
    valueLink = {
      value: this.state.term,
      requestChange: this.handleChange
    };

    return <input type="search" autoFocus="autofocus" valueLink={valueLink} />;
  }
});