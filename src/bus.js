// STATE <==== UI

var Bacon = require('baconjs').Bacon;

var searchTerm = new Bacon.Bus;

module.exports = {
  searchTerm: searchTerm
};