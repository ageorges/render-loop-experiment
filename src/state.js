// STATE

var Bacon = require('baconjs').Bacon;
var $ = require('jquery');

var bus = require('./bus');

var searchTerm = bus.searchTerm.toProperty('yo');

var results = searchTerm.changes().throttle(500).flatMapLatest(function(term) {
  return Bacon.fromPromise($.ajax({
    // url: 'https://marketplace.atlassian.com/rest/1.0/plugins/search',
    url: 'http://localhost:8080/rest/1.0/plugins/search',
    data: { q: term }
  }));
});

var loading = searchTerm.map(true).changes().merge(results.map(false).mapError(false)).skipDuplicates().toProperty(false);

var links = results.map(function(r) { return r.links }).toProperty([]);
var plugins = results.map(function(r) { return r.plugins; }).toProperty([]);

module.exports = Bacon.combineTemplate({
  searchTerm: searchTerm,
  loading: loading,
  links: links,
  plugins: plugins
});
