var morgan = require('morgan');
var bodyParser = require('body-parser');
var express = require('express');
var app = express()

app.use(morgan('dev'));
app.use(bodyParser());
app.use(express.static(__dirname), '/dist');

app.set('views', process.cwd());
app.set('view engine', 'ejs');

app.use('/', require('./routes'));

app.listen(9000, function() {
  console.log('Listening on port 9000');
});